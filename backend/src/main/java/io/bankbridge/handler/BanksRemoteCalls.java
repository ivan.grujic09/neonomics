package io.bankbridge.handler;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.bankbridge.model.BankModel;
import spark.Request;
import spark.Response;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.URI;

public class 	BanksRemoteCalls {

	private static Map config;
	private static HttpClient client;

	public static void init() throws Exception {
		config = new ObjectMapper()
				.readValue(Thread.currentThread().getContextClassLoader().getResource("banks-v2.json"), Map.class);
	}

	public static String handle(Request request, Response response) {
		List<Map> result = new ArrayList<>();
		for (Object entry : config.values()) {
			String url = (String) entry;
			Map bank = retrieveBank(url);
			result.add(bank);
		}

		try {
			String resultAsString = new ObjectMapper().writeValueAsString(result);
			return resultAsString;
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Error while processing request");
		}
	}

	private static Map retrieveBank(String urlString) {
		Map map = new HashMap<>();
		try{
			URL url = new URL(urlString);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestProperty("accept", "application/json");

			BankModel bank = null;
			InputStream responseStream = connection.getInputStream();

			ObjectMapper mapper = new ObjectMapper();
			bank = mapper.readValue(responseStream, BankModel.class);
			map = new HashMap<>();
			map.put("id", bank.bic);
			map.put("bank", bank);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

}
