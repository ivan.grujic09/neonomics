package io.bankbridge;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.before;

import io.bankbridge.handler.BanksCacheBased;
import io.bankbridge.handler.BanksRemoteCalls;
import spark.Filter;
import spark.Service;

public class Main {

	public static void main(String[] args) throws Exception {
		
		port(8080);

		before((Filter) (request, response) -> {
			response.header("Access-Control-Allow-Origin", "*");
			response.header("Access-Control-Allow-Methods", "GET");
		});

		BanksCacheBased.init();
		BanksRemoteCalls.init();
		
		get("/v1/banks/all", (request, response) -> BanksCacheBased.handle(request, response));
		get("/v2/banks/all", (request, response) -> BanksRemoteCalls.handle(request, response));
	}
}