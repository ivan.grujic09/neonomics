package io.bankbridge.tests;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestAPI {

    @Test
    public void simpleTestV1() throws IOException {
        List<String> bics = new ArrayList<String>();
        bics.add("PARIATURDEU0XXX");
        bics.add("CUPIDATATSP1XXX");
        bics.add("DOLORENOR2XXX");
        bics.add("DESERUNTSP3XXX");
        bics.add("MOLLITNOR4XXX");
        bics.add("MOLLITSWE5XXX");
        bics.add("REPSP6XXX");
        bics.add("ANIMDEU7XXX");
        bics.add("DODEU8XXX");
        bics.add("DOLORENOR9XXX");
        bics.add("CONSSWE10XXX");
        bics.add("NONNOR11XXX");
        bics.add("NSAVNOR12XXX");
        bics.add("MOLLITSP13XXX");
        bics.add("VELITDEU14XXX");
        bics.add("FIRSTSP15XXX");
        bics.add("ULLAMCOSP16XXX");
        bics.add("LNBSP17XXX");
        bics.add("SOARCDEU18XXX");
        bics.add("ETSWE19XXX");

        URL url = new URL("http://localhost:8080/v1/banks/all");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");

        List<Map> banks = null;
        InputStream responseStream = connection.getInputStream();

        ObjectMapper mapper = new ObjectMapper();
        banks = mapper.readValue(responseStream, List.class);

        for (Map bank : banks) {
            LinkedHashMap bankModel = (LinkedHashMap) bank.get("bank");
            assertEquals(true, bics.contains(bankModel.get("bic")));
        }

        assertEquals(bics.size(), banks.size());
    }

    @Test
    public void simpleTestV2() throws IOException {
        List<String> bics = new ArrayList<String>();
        bics.add("PARIATURDEU0XXX");
        bics.add("CUPIDATATSP1XXX");
        bics.add("DOLORENOR2XXX");
        bics.add("DESERUNTSP3XXX");
        bics.add("MOLLITNOR4XXX");
        bics.add("MOLLITSWE5XXX");
        bics.add("REPSP6XXX");
        bics.add("ANIMDEU7XXX");
        bics.add("DODEU8XXX");
        bics.add("DOLORENOR9XXX");
        bics.add("CONSSWE10XXX");
        bics.add("NONNOR11XXX");
        bics.add("NSAVNOR12XXX");
        bics.add("MOLLITSP13XXX");
        bics.add("VELITDEU14XXX");
        bics.add("FIRSTSP15XXX");
        bics.add("ULLAMCOSP16XXX");
        bics.add("NULLASP17XXX");
        bics.add("SOARCDEU18XXX");
        bics.add("ETSWE19XXX");

        URL url = new URL("http://localhost:8080/v2/banks/all");

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("accept", "application/json");

        List<Map> banks = null;
        InputStream responseStream = connection.getInputStream();

        ObjectMapper mapper = new ObjectMapper();
        banks = mapper.readValue(responseStream, List.class);

        for (Map bank : banks) {
            LinkedHashMap bankModel = (LinkedHashMap) bank.get("bank");
            assertEquals(true, bics.contains(bankModel.get("bic")));
        }

        assertEquals(bics.size(), banks.size());
    }
}
