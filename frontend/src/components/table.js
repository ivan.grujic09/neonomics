import React from 'react';
import { connect } from 'react-redux';

class Table extends React.Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        if (this.props.banks.api == "" || this.props.banks.api == undefined) {
            return (<div></div>);
        }
        let rows = [];
        
        let itemsPerPage = this.props.table.itemsPerPage;
        let tablePage = this.props.table.tablePage;
        let startIndes = itemsPerPage * tablePage;
        let endIndex = itemsPerPage * (tablePage + 1);

        if (this.props.banks.data != undefined) {
            for (let bank of this.props.banks.data.slice(startIndes, endIndex)) {
                rows.push(
                    <tr key={bank.id}>
                        <td>{bank.id}</td>
                        <td>{bank.bank.name}</td>
                        <td>{bank.bank.countryCode}</td>
                        <td>{bank.bank.auth}</td>
                        <td hidden={this.props.banks.api === "v2"}>{bank.bank.products ? bank.bank.products.join() : ""}</td>
                    </tr>
                )
            }   
        }
        return (
            <span>
            <div className="row">
                <div className="col-12">
                    <h1>List of Banks</h1>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <p>Bank(s)</p>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <table className="table table-bordered">
                        <thead className="table-light">
                            <tr>
                                <th style={{width: "15%"}}> BIC </th>
                                <th style={{width: "35%"}}> Name </th>
                                <th style={{width: "10%"}}> Country Code </th>
                                <th style={{width: "20%"}}> Auth </th>
                                <th hidden={this.props.banks.api === "v2"} style={{width: "20%"}}> Products </th>
                            </tr>
                        </thead>
                        <tbody>
                            {rows}
                        </tbody>
                    </table>
                </div>
            </div>
            </span>
        );
    }
}

const mapStateToProps = (state) => ({
    banks: state.banks,
    table: state.table
})

export default connect(mapStateToProps)(Table);
