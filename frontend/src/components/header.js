import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { fetchBanks, fetchBanksSuccess, fetchBanksError, apiVersionChangeAction } from '../actions/bankActions';
import { tableFirstPageAction, tableItemsPerPageChangedAction } from '../actions/tableActions';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: 0
        }

        this.onAPIChange = this.onAPIChange.bind(this);
    }

    async onAPIChange(event) {
        let selectedAPIVersion = event.target.value;

        this.props.dispatch(apiVersionChangeAction(selectedAPIVersion));
        this.props.dispatch(tableFirstPageAction(0));

        if (selectedAPIVersion != "") {
            this.props.dispatch(fetchBanks(selectedAPIVersion));
            await axios.get(
                "http://localhost:8080/"+selectedAPIVersion+"/banks/all"
            ).then(response => {
                this.props.dispatch(fetchBanksSuccess(response.data));
            }).catch(error => {
                this.props.dispatch(fetchBanksError(error));
            });
        }


        this.setState({
            selected: selectedAPIVersion
        });
    }
    
    render() {
        return (
            <div className="row header">
                <div className="col-12">
                    <div className="row">
                        <div className="col-2 d-flex align-items-center flex-row">
                            <span>Select the API version:</span>
                        </div>
                        <div className="col-3 d-flex align-items-center flex-row">
                            <select onChange={this.onAPIChange} defaultValue="0" className="form-select" aria-label="Default select example">
                                <option value="">Select an API version</option>
                                <option value="v1">v1 API Test</option>
                                <option value="v2">v2 API Test</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect()(Header);
