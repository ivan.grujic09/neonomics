import React from 'react';
import { connect } from 'react-redux';
import {
    tableItemsPerPageChangedAction,
    tableNextPageAction,
    tableLastPageAction,
    tablePrevPageAction,
    tableFirstPageAction
} from '../actions/tableActions';

class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.itemsPerPageChanged = this.itemsPerPageChanged.bind(this);
        this.lastPageOnClick = this.lastPageOnClick.bind(this);
        this.nextPageOnClick = this.nextPageOnClick.bind(this);
        this.prevPageOnClick = this.prevPageOnClick.bind(this);
        this.firstPageOnClick = this.firstPageOnClick.bind(this);
    }

    itemsPerPageChanged(e) {
        this.props.dispatch(tableItemsPerPageChangedAction(parseInt(e.target.value)));
        this.firstPageOnClick(null);
    }

    lastPageOnClick(e) {
        this.props.dispatch(tableLastPageAction(Math.ceil(this.props.banks.data.length / this.props.table.itemsPerPage) - 1));
    }

    nextPageOnClick(e) {
        this.props.dispatch(tableNextPageAction(parseInt(this.props.table.tablePage) + 1));
    }
    
    prevPageOnClick(e) {
        this.props.dispatch(tablePrevPageAction(parseInt(this.props.table.tablePage) - 1));
    }

    firstPageOnClick(e) {
        this.props.dispatch(tableFirstPageAction(0));
    }

    render() {
        if (this.props.banks.api == "" || this.props.banks.api == undefined) {
            return (<div></div>);
        }

        let banksCount = this.props.banks.data.length;
        let currentPage = this.props.table.tablePage;

        let low = currentPage * this.props.table.itemsPerPage + 1;
        let high = Math.min(low + this.props.table.itemsPerPage - 1, banksCount);

        return (
            <div className="row">
                <div className="col-6 d-flex align-items-center flex-row">
                    <span className="nav-text">Items per page:</span>
                    <select className="form-select nav-items-per-page" aria-label="Default select example" onChange={this.itemsPerPageChanged}>
                        <option value="5">5</option>
                        <option value="7">7</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="42">42</option>
                    </select>
                    <span className="nav-text">{low}-{high} of {banksCount}</span>
                </div>
                <div className="nav-buttons-div col-6 d-flex align-items-center flex-row-reverse">
                    <button disabled={high >= banksCount} type="button" className="btn btn-outline-dark nav-button" onClick={this.lastPageOnClick}>
                        <i className="fa fa-angle-double-right" aria-hidden="true"></i>
                    </button>
                    <button disabled={high >= banksCount} type="button" className="btn btn-outline-dark nav-button" onClick={this.nextPageOnClick}>
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </button>
                    <button disabled={this.props.table.tablePage == 0} type="button" className="btn btn-outline-dark nav-button" onClick={this.prevPageOnClick}>
                        <i className="fa fa-angle-left" aria-hidden="true"></i>
                    </button>
                    <button disabled={this.props.table.tablePage == 0} type="button" className="btn btn-outline-dark nav-button" onClick={this.firstPageOnClick}>
                        <i className="fa fa-angle-double-left" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    banks: state.banks,
    table: state.table
})

export default connect(mapStateToProps)(Navigation);
