export const BANKS_ACTIONS = {
	FETCH_BANKS: "FETCH_BANKS",
	FETCH_BANKS_SUCCESS: "FETCH_BANKS_SUCCESS",
	FETCH_BANKS_ERROR: "FETCH_BANKS_ERROR",
    API_VERSION_CHANGED: "API_VERSION_CNAHGED"
}

export const fetchBanks = function fetchBanks(api) {
    return {
        type: BANKS_ACTIONS.FETCH_BANKS,
        payload: {
            api
        }
    }
}

export const fetchBanksSuccess = function fetchBanksSuccess(banks) {
    return {
        type: BANKS_ACTIONS.FETCH_BANKS_SUCCESS,
        payload: {
            banks
        }
    }
}

export const fetchBanksError = function fetchBanksError(error) {
    return {
        type: BANKS_ACTIONS.FETCH_BANKS_ERROR,
        payload: {
            error
        }
    }
}

export const apiVersionChangeAction = function apiVersionChangeAction(apiVersion) {
    return {
        type: BANKS_ACTIONS.API_VERSION_CHANGED,
        payload: {
            apiVersion
        }
    }
}