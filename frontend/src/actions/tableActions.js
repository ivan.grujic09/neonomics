export const TABLE_ACTIONS = {
	TABLE_ITEMS_PER_PAGE_CHANGED:   "TABLE_ITEMS_PER_PAGE_CHANGED",
	TABLE_NEXT_PAGE:                "TABLE_NEXT_PAGE",
	TABLE_LAST_PAGE:                "TABLE_LAST_PAGE",
	TABLE_PREV_PAGE:                "TABLE_PREV_PAGE",
	TABLE_FIRST_PAGE:               "TABLE_FIRST_PAGE",
}

export const tableItemsPerPageChangedAction = function tableItemsPerPageChangedAction(items) {
    return {
        type: TABLE_ACTIONS.TABLE_ITEMS_PER_PAGE_CHANGED,
        payload: {
            items
        }
    }
}

export const tableNextPageAction = function tableNextPageAction(tablePage) {
    return {
        type: TABLE_ACTIONS.TABLE_NEXT_PAGE,
        payload: {
            tablePage
        }
    }
}

export const tableLastPageAction = function tableLastPageAction(tablePage) {
    return {
        type: TABLE_ACTIONS.TABLE_LAST_PAGE,
        payload: {
            tablePage
        }
    }
}

export const tablePrevPageAction = function tablePrevPageAction(tablePage) {
    return {
        type: TABLE_ACTIONS.TABLE_PREV_PAGE,
        payload: {
            tablePage
        }
    }
}

export const tableFirstPageAction = function tableFirstPageAction(tablePage) {
    return {
        type: TABLE_ACTIONS.TABLE_FIRST_PAGE,
        payload: {
            tablePage
        }
    }
}