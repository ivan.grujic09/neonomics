import { TABLE_ACTIONS } from '../actions/tableActions'

const intialState = {
    itemsPerPage: 5,
    tablePage: 0
}

const tableReducer = function (state = intialState, action) {
    switch (action.type) {
        case TABLE_ACTIONS.TABLE_ITEMS_PER_PAGE_CHANGED:
            return {
                ... state,
                itemsPerPage: action.payload.items
            }
        case TABLE_ACTIONS.TABLE_NEXT_PAGE:
        case TABLE_ACTIONS.TABLE_FIRST_PAGE:
        case TABLE_ACTIONS.TABLE_LAST_PAGE:
        case TABLE_ACTIONS.TABLE_PREV_PAGE:
            return {
                ... state,
                tablePage: action.payload.tablePage
            }
        default:
            return state;
    }
}

export default tableReducer;
