import banksReducer from './bankReducer.js';
import tableReducer from './tableReducer.js';
import { combineReducers } from 'redux';

const reducers = combineReducers({
    banks: banksReducer,
    table: tableReducer
});

export default reducers;