import { BANKS_ACTIONS } from '../actions/bankActions'

const initialState = {
    data: []
}

const banksReducer = function (state = initialState, action) {
    switch (action.type) {
        case BANKS_ACTIONS.FETCH_BANKS:
            return {
                ... state
            }
        case BANKS_ACTIONS.FETCH_BANKS_SUCCESS:
            return {
                ... state,
                data: action.payload.banks
            }
        case BANKS_ACTIONS.FETCH_BANKS_ERROR:
            return {
                ... state
            }
        case BANKS_ACTIONS.API_VERSION_CHANGED:
            return {
                ... state,
                api: action.payload.apiVersion,
                data: []
            }
        default:
            return state;
    }
}

export default banksReducer;
