import './App.css';
import Header from './components/header';
import Table from './components/table';
import Navigation from './components/navigation';

function App() {
  return (
    <div className="App container">
      <Header />
      <Table />
      <Navigation />
    </div>
  );
}

export default App;
